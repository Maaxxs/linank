# linank

[![Crates.io](https://img.shields.io/crates/l/linank)](./LICENSE)
[![Rust Docs](https://docs.rs/linank/badge.svg)](https://docs.rs/linank)

## Install

If you've got `cargo` installed

```sh
cargo install linank
```

You can also build it youself

```sh
git clone https://gitlab.com/Maaxxs/linank.git
cd linank/
cargo install --path .
```

## Notes regarding Anki

- You need the [Anki Connect Addon](https://ankiweb.net/shared/info/2055492159) installed
- Anki needs to be running while you execute this program (the Anki Connect Addon is listening on `localhost:8765`)

## Usage

Note: The _deck name_ in Anki must already exist. Maybe I'll implement auto creation for non existent decks.

Note: At the moment the translation is _english->german_ and hardcoded. That may change as well.

`linank --help`

```sh
Linank 1.0.0
Max Kunzelmann <maxdev@posteo.de>
Searches a tranlation at linguee.de and adds it to an Anki deck.

USAGE:
    linank [FLAGS] [OPTIONS] <query>

FLAGS:
    -h, --help
            Prints help information

    -q, --quiet
            Pass many times for less log output

    -V, --version
            Prints version information

    -v, --verbose
            Pass many times for more log output

            By default, it will only report errors. Passing `-v` one time also prints warnings, `-vv` enables info
            logging, `-vvv` debug, and `-vvvv` trace.

OPTIONS:
    -d, --deck-name <deck-name>
            The name of the deck in Anki [default: Englisch]


ARGS:
    <query>
            The word which should be translated
```

### Example

```sh
$ linank precious
Suche: precious
Bedeutung als Adjektiv: "precious"
    kostbar
    edel
    wertvoll
Bedeutung als Substantiv: "precious"
    Liebling
Zu Anki hinzufügen?[Y/n]
```

```sh
$ linank "precious time"
Suche: precious time
Bedeutung als Substantiv: "precious time"
    kostbare Zeit
    wertvolle Zeit
Zu Anki hinzufügen?[Y/n]
```

Add the word to a different deck than _Englisch_.

```sh
$ linank -d TestDeck precious
Suche: precious
Bedeutung als Adjektiv: "precious"
    kostbar
    edel
    wertvoll
Bedeutung als Substantiv: "precious"
    Liebling
Zu Anki hinzufügen?[Y/n]
```

## [Linguee](https://linguee.de)

I wrote this program only for personal use.

Plase read their [Terms and Conditions](https://www.linguee.com/english-russian/page/termsAndConditions.php) and [Privacy statement](https://www.linguee.com/english-russian/page/privacy.php).

## License

[MIT](LICENSE)
