use crate::linguee::Meaning;
use log::info;
use reqwest;
use serde::Deserialize;
use serde_json::json;

const ANKI_URL: &str = "http://localhost:8765";

/// Create the json `String` which should be send to the Anki server.
pub fn create_json(
    meanings: &[Meaning],
    deck_name: &str,
) -> Result<String, serde_json::error::Error> {
    // first, check the connection to anki
    check_connection();

    let result = json!({
        "action": "addNote",
        "version": 6,
        "params": {
            "note": {
                "deckName": deck_name,
                "modelName": "Basic",
                "fields": {
                    "Front": note_front(meanings),
                    "Back": note_back(meanings),
                },
                "options": {
                    "allowDuplicate": false
                },
                "tags": [],
        }
    }
    })
    .to_string();
    Ok(result)
}

/// The Anki json response
#[derive(Debug, Deserialize)]
struct AnkiResponse {
    /// Contains the successfull return value
    result: Option<u64>,

    /// Contains the error string, 
    /// None if there is no error
    error: Option<String>,
}

/// Return a representation `String` for the back of the Anki note
fn note_back(meanings: &[Meaning]) -> String {
    let mut result = String::new();
    for meaning in meanings {
        result.push_str(
            format!(
                "<div style=\"font-size: 14px;\">{}</div><br>",
                meaning.word_type
            )
            .as_str(),
        );

        for text in &meaning.german {
            result.push_str(text);
            result.push_str("<br>");
        }
        result.push_str("<br>");
    }
    result
}

/// Return a representation `String` for the front of the Anki note
fn note_front(meanings: &[Meaning]) -> String {
    let mut result = String::new();

    for meaning in meanings {
        if !result.contains(&meaning.english) {
            result.push_str(&meaning.english);
            result.push_str("<br>");
        }
    }
    result
}

/// Send the given json to the Anki python3 server.
///
/// If anything goes wrong, this function panics.
pub fn send(json: String) {
    let client = reqwest::blocking::Client::new();
    let res = client
        .post(ANKI_URL)
        .body(json)
        .send()
        .expect("Failed to send request to anki");

    info!("Anki response:\n{:?}", res);

    let body = res
        .text()
        .expect("Could not get the body of the anki json response");

    let response: AnkiResponse =
        serde_json::from_str(&body).expect("Could not deserialize json from anki response");
    if response.result.is_some() {
        println!("Erfolgreich zu Anki hinzugefügt");
    } else {
        eprintln!(
            "Konnte es nicht hinzufügen.\nError: {}",
            response.error.unwrap()
        );
    }
}

/// Send a simple `action: "version"` to Anki to see if the python3
/// server of the Anki Addon is running.
///
/// It does not parse the returning json, so this function just checks
/// if *something* is listening on localhost:8765
///
/// This function exits if it cannot establish a connection
fn check_connection() {
    let client = reqwest::blocking::Client::new();
    let body = json!({
        "action": "version",
        "version": 6,
    })
    .to_string();

    match client.post(ANKI_URL).body(body).send() {
        Ok(_) => (),
        Err(err) => {
            eprintln!("\nCannot establish a connection to Anki.");
            eprintln!(
            "Did you install the Anki Connect Addon? https://ankiweb.net/shared/info/2055492159"
        );
            eprintln!("Is Anki running?");
            eprintln!("Error: {}", err);
            std::process::exit(1);
        }
    }
}
