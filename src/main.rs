use clap_verbosity_flag::Verbosity;
use linank::anki;
use linank::linguee::{translate, Meaning, TranslateError};
use log::{debug, info};
use simple_logger;
use std::io::{self, Write};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(
    name = "Linank",
    about = "Searches a tranlation at linguee.de and adds it to an Anki deck.",
    author = "Max Kunzelmann <maxdev@posteo.de>"
)]
struct CliArgs {
    /// The word which should be translated
    query: String,

    /// The name of the deck in Anki
    #[structopt(short = "d", long = "deck-name", default_value = "Englisch")]
    deck_name: String,

    #[structopt(flatten)]
    verbose: Verbosity,
}

fn main() {
    let args = CliArgs::from_args();
    simple_logger::init_with_level(args.verbose.log_level().unwrap_or(log::Level::Error))
        .expect("Initializing the logger failed.");

    info!("{:?}", args);

    let meanings = match translate(&args.query) {
        Ok(val) => val,
        Err(TranslateError::DidYouMean(word)) => ask_requery(word),
        Err(TranslateError::NoResult) => {
            println!("Keine Ergebnisse gefunden");
            std::process::exit(0);
        }
    };
    debug!("{:?}", meanings);

    print!("Zu Anki hinzufügen?[Y/n] ");
    io::stdout().flush().expect("Error in flush");

    let mut answer = String::new();
    io::stdin().read_line(&mut answer).unwrap_or_else(|err| {
        eprintln!("\nSomething went wrong reading your input.\n{}", err);
        std::process::exit(1);
    });

    let answer = answer.trim().to_lowercase();
    if answer.is_empty() || answer.eq("y") {
        let json = anki::create_json(&meanings, &args.deck_name)
            .expect("serde json could not create json string.");
        anki::send(json);
    } else {
        println!("Ok, beende");
    }
}

fn ask_requery(word: String) -> Vec<Meaning> {
    print!("Meintest du \"{}\"? [Y/n]", word);
    io::stdout()
        .flush()
        .expect("Error in flushing to command line");
    let mut answer = String::new();
    io::stdin().read_line(&mut answer).unwrap_or_else(|err| {
        panic!("Something went wrong reading your input.\n{}", err);
    });
    let answer = answer.trim();
    if answer.is_empty() || answer.to_lowercase().eq("y") {
        translate(&word).expect("This should never fail as we have a suggested word from linguee")
    } else {
        println!("Ok, beende");
        std::process::exit(0);
    }
}
