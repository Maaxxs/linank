use log::info;
use reqwest;
use scraper::{Html, Selector};

const BASE_URL: &str = "https://www.linguee.de/deutsch-englisch/search?source=en&query=";

#[derive(Debug, Default)]
pub struct Meaning {
    /// Word type such as noun, verb, adjective,...
    pub word_type: String,

    /// The english word with preposition if any
    pub english: String,

    /// The german meanings
    pub german: Vec<String>,
}

impl Meaning {
    /// Create a new Meaning
    pub fn new() -> Meaning {
        Meaning {
            word_type: String::new(),
            english: String::new(),
            german: Vec::new(),
        }
    }

    /// Print the meanings to stdout
    pub fn show(&self) {
        println!("Bedeutung als {}: \"{}\"", self.word_type, self.english);
        for meaning in &self.german {
            println!("    {}", meaning);
        }
    }
}

/// Error while getting the translation for a query
#[derive(Debug)]
pub enum TranslateError {
    /// Found a suggestion "did you mean" on linguee
    ///
    /// If this is found, it is always returnd so that the caller
    /// can request a new translation for the corrected query.
    DidYouMean(String),

    /// Found no results for query on linguee
    NoResult,
}

/// Look for the tag span with class corrected. This indicates a typo
/// in our query.
pub fn did_you_mean(doc: &Html) -> Option<String> {
    let sel_span = Selector::parse(r#"span[class="corrected"]"#).unwrap();

    if let Some(val) = doc.select(&sel_span).next() {
        Some(val.inner_html())
    } else {
        None
    }
}

/// Check the html if it contains results
pub fn has_results(doc: &Html) -> bool {
    // Todo: maybe check if <div class="lemma featured"> exist. If no featured
    // meanings exist, return false
    let sel_noresults = Selector::parse(r#"h1[class="noresults wide_in_main"]"#).unwrap();
    if doc.select(&sel_noresults).next().is_none() {
        return true;
    }
    false
}

/// Translate query
///
/// # Panics
///
/// - If a connection to Linguee cannot be established.
/// - If it cannot extract a specific html element. These cases have .except() calls.
///
pub fn translate(query: &str) -> Result<Vec<Meaning>, TranslateError> {
    let url = format!("{}{}", BASE_URL, query);
    info!("Query URL {}", url);

    let content = match reqwest::blocking::get(&url) {
        Ok(response) => response
            .text()
            .expect("Could not get text from response body."),
        Err(err) => panic!("Could not connect to linguee.\nError: {}", err),
    };

    let doc = Html::parse_document(&content);

    if !has_results(&doc) {
        return Err(TranslateError::NoResult);
    }

    if let Some(word) = did_you_mean(&doc) {
        return Err(TranslateError::DidYouMean(word));
    }

    // Only look at the foreign term div, so englisch to german translations and not
    // german to english translations
    let sel_source_lang_en = Selector::parse(r#"div[class="isForeignTerm"]"#).unwrap();
    // container which hold the featured translations
    let sel_div = Selector::parse(r#"div[class="lemma featured"]"#).unwrap();
    // h2 is the word in english
    let sel_h2 = Selector::parse("h2").unwrap();
    // h3 are the translations for h2
    let sel_h3 = Selector::parse("h3").unwrap();
    // Sometimes is a grammar info in the translation which we don't need
    let sel_grammar_info = Selector::parse(r#"span[class="grammar_info"]"#).unwrap();
    // word type like noun, verb, adjective, ...
    let sel_word_type = Selector::parse(r#"span[class="tag_wordtype"]"#).unwrap();
    // placeholder is usually "sth" or "sb" in the h2 word.
    let sel_placeholder = Selector::parse(r#"span[class="placeholder"]"#).unwrap();
    // link to a translation of a word. Needed if the english word h2 consists of
    // multiple words, so every word has its own clickable link.
    // Example: "turn out" contains a link on "turn" and a link on "out" to their
    // corresponding translation. To capture "turn out" we need the text of both links.
    let sel_a = Selector::parse("a").unwrap();
    // Contains the links of single words and the word type
    let sel_tag_lemma = Selector::parse(r#"span[class="tag_lemma"]"#).unwrap();

    let mut meanings = Vec::new();
    for div in doc
        .select(&sel_source_lang_en)
        .next()
        .unwrap()
        .select(&sel_div)
    {
        let mut meaning = Meaning::new();
        let h2 = div.select(&sel_h2).next().expect("Could not find h2.");

        // The class "tag_lemma" shoud always be there, so we can unwrap it.
        for link in h2.select(&sel_tag_lemma).next().unwrap().select(&sel_a) {
            for text in link.text() {
                meaning.english.push_str(text.trim());
                meaning.english.push_str(" ");
            }
        }
        meaning.english = meaning.english.trim_end().to_string();

        if let Some(tag_type) = h2.select(&sel_word_type).next() {
            meaning.word_type = tag_type
                .text()
                .next()
                .expect("Could not unwrap text of word type")
                .to_string()
        };

        for h3 in div.select(&sel_h3) {
            if h3.select(&sel_grammar_info).next().is_some() {
                let text = format!(
                    "{}{}",
                    h3.text().next().expect("Could not find preposition"),
                    h3.text().nth(2).expect("Could not find meaning"),
                );
                meaning.german.push(text);
                continue;
            }

            if h3.select(&sel_placeholder).next().is_some() {
                let mut text = h3
                    .text()
                    .next()
                    .expect("Could not find preposition")
                    .to_string();
                text.push_str(h3.text().nth(1).expect("Could not find meaning"));
                meaning.german.push(text);
                continue;
            }

            meaning.german.push(
                h3.text()
                    .next()
                    .expect("Could not find meaning")
                    .to_string(),
            );
        }
        meaning.show();
        meanings.push(meaning);
    }
    Ok(meanings)
}

// /// This function just saves queries to {query}.html
// pub fn save_html(query: &str) {
//     let url = format!("{}{}", BASE_URL, query);
//     println!("querying: {}", url);
//     let content = reqwest::blocking::get(&url).unwrap().text().unwrap();
//     let filename = format!("{}.html", query);
//     let mut file = File::create(&filename).unwrap();
//     file.write_all(&content.as_bytes()).unwrap();
//     println!("Saved {}. Exit now", &filename);
//     std::process::exit(0);
// }

/*
#[test]
fn linguee_didyoumean_misspelled() {
    let content = get_html_local("poisn").unwrap();
    let doc = Html::parse_document(&content);
    let result = did_you_mean(&doc);
    assert_eq!(Some("poison".to_string()), result);
}

#[test]
fn linguee_didyoumean_correct() {
    let content = get_html_local("poison").unwrap();
    let doc = Html::parse_document(&content);
    let result = did_you_mean(&doc);
    assert_eq!(None, result);
}
*/
